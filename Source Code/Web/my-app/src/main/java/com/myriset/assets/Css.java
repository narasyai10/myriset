/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.assets;

import org.jooby.Jooby;

/**
 *
 * @author Narasyai10
 */
public class Css extends Jooby {
    {
        assets("/admin/css/style.css", "/admin/css/style.css");
        assets("/admin/css/themes/all-themes.css", "/admin/css/themes/all-themes.css");
        assets("/admin/css/materialize.css", "/admin/css/materialize.css");

        //user
        assets("/fishery/css/responsive.css", "/fishery/css/responsive.css");
        assets("/fishery/css/presets/preset1.css", "/fishery/css/presets/preset1.css");
        assets("/fishery/css/lightbox.css", "/fishery/css/lightbox.css");
        assets("/fishery/css/animate.min.css", "/fishery/css/animate.min.css");
        assets("/fishery/css/main.css", "/fishery/css/main.css");
        assets("/fishery/css/font-awesome.min.css", "/fishery/css/font-awesome.min.css");
        assets("/fishery/css/bootstrap.min.css", "/fishery/css/bootstrap.min.css");
        assets("/admin/plugins/bootstrap/css/bootstrap.css.map", "/admin/plugins/bootstrap/css/bootstrap.css.map");
        assets("/admin/plugins/bootstrap-select/css/bootstrap-select.css", "/admin/plugins/bootstrap-select/css/bootstrap-select.css");
    }
    
}