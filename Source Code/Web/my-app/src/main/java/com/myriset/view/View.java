/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.view;

import org.jooby.Jooby;

/**
 *
 * @author Narasyai10
 */
public class View extends Jooby {
    {
      	assets("/index", "index.html");
        assets("/input.html", "input.html");
      	assets("/admin/index.html", "/admin/index.html");
        assets("/admin/pages/views/sumberdaya.html", "/admin/pages/views/sumberdaya.html");
        assets("/admin/pages/views/daerah.html", "/admin/pages/views/daerah.html");
        assets("/admin/pages/views/sarana.html", "/admin/pages/views/sarana.html");
        assets("/admin/pages/views/ekonomi.html", "/admin/pages/views/ekonomi.html");
        assets("/admin/pages/views/feedback.html", "/admin/pages/views/feedback.html");
        assets("/admin/pages/views/bantuan.html", "/admin/pages/views/bantuan.html");
        assets("/admin/pages/views/sign-in.html", "/admin/pages/views/sign-in.html");
        assets("/admin/pages/views/sign-up.html", "/admin/pages/views/sign-up.html");

        assets("/fishery/index.html", "/fishery/index.html");
    }
}
