/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.assets;

import org.jooby.Jooby;

/**
 *
 * @author Narasyai10
 */
public class Js extends Jooby{
    {
        assets("/admin/js/admin.js", "/admin/js/admin.js");
        assets("/admin/js/pages/index.js", "/admin/js/pages/index.js");
        assets("/admin/js/demo.js", "/admin/js/demo.js");
        assets("/admin/js/pages/tables/jquery-datatable.js", "/admin/js/pages/tables/jquery-datatable.js");
        assets("/admin/js/pages/examples/sign-in.js", "/admin/js/pages/examples/sign-in.js");
        assets("/admin/js/pages/examples/sign-up.js", "/admin/js/pages/examples/sign-up.js");
        assets("/admin/js/pages/ui/dialogs.js", "/admin/js/pages/ui/dialogs.js");
        assets("/admin/js/pages/charts/chartjs.js", "/admin/js/pages/charts/chartjs.js");

        //user
        assets("/fishery/js/main.js", "/fishery/js/main.js");
        assets("/fishery/js/lightbox.min.js", "/fishery/js/lightbox.min.js");
        assets("/fishery/js/jquery.countTo.js", "/fishery/js/jquery.countTo.js");
        assets("/fishery/js/smoothscroll.js", "/fishery/js/smoothscroll.js");
        assets("/fishery/js/mousescroll.js", "/fishery/js/mousescroll.js");
        assets("/fishery/js/wow.min.js", "/fishery/js/wow.min.js");
        assets("/fishery/js/jquery.inview.min.js", "/fishery/js/jquery.inview.min.js");
        assets("/fishery/js/bootstrap.min.js", "/fishery/js/bootstrap.min.js");
        assets("/fishery/js/jquery.js", "/fishery/js/jquery.js");
        assets("/fishery/js/jquery.inview.min.js", "/fishery/js/jquery.inview.min.js");
        assets("/fishery/js/lightbox.min.map", "/fishery/js/lightbox.min.map");
    }
}