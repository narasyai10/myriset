/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.assets;

import org.jooby.Jooby;

/**
 *
 * @author Narasyai10
 */
public class Images extends Jooby {
    {
        assets("/images/user.png", "/images/user.png");
        assets("/admin/favicon.ico", "/admin/favicon.ico");
        assets("/admin/images/user-img-background.jpg", "/admin/images/user-img-background.jpg");
        assets("/admin/images/user-img-background1.jpg", "/admin/images/user-img-background1.jpg");
        assets("/admin/images/logo.jpg", "/admin/images/logo.jpg");
        assets("/admin/images/user.png", "/admin/images/user.png");
        assets("/admin/images/help1.png", "/admin/images/help1.png");
        assets("/admin/images/help2.png", "/admin/images/help2.png");
        assets("/admin/images/help3.png", "/admin/images/help3.png");
        assets("/admin/images/help4.png", "/admin/images/help4.png");
        assets("/admin/images/help5.png", "/admin/images/help5.png");
        assets("/admin/images/help6.png", "/admin/images/help6.png");
        assets("/admin/images/help7.png", "/admin/images/help7.png");
        assets("/admin/images/help8.png", "/admin/images/help8.png");
        assets("/admin/images/help9.png", "/admin/images/help9.png");

        //user
        assets("/fishery/images/favicon.ico", "/fishery/images/favicon.ico");
        assets("/fishery/images/slider/slider1.jpg", "/fishery/images/slider/slider1.jpg");
        assets("/fishery/images/slider/slider2.jpg", "/fishery/images/slider/slider2.jpg");
        assets("/fishery/images/slider/slider3.jpg", "/fishery/images/slider/slider3.jpg");
        assets("/fishery/images/team/1.jpg", "/fishery/images/team/1.jpg");
        assets("/fishery/images/team/2.jpg", "/fishery/images/team/2.jpg");
        assets("/fishery/images/team/3.jpg", "/fishery/images/team/3.jpg");
        assets("/fishery/images/team/4.jpg", "/fishery/images/team/4.jpg");
        assets("/fishery/images/logo.png", "/fishery/images/logo.png");
        assets("/fishery/images/logo1.png", "/fishery/images/logo1.png");
        assets("/fishery/images/lightbox/next.png", "/fishery/images/lightbox/next.png");
        assets("/fishery/images/lightbox/loading.gif", "/fishery/images/lightbox/loading.gif");
        assets("/fishery/images/lightbox/prev.png", "/fishery/images/lightbox/prev.png");
        assets("/fishery/images/lightbox/close.png", "/fishery/images/lightbox/close.png");
        assets("/fishery/images/contact-bg.jpg", "/fishery/images/contact-bg.jpg");
        assets("/fishery/images/blog/1.jpg", "/fishery/images/blog/1.jpg");

        assets("/fishery/images/bantuan1.png", "/fishery/images/bantuan1.png");
        assets("/fishery/images/bantuan2.png", "/fishery/images/bantuan2.png");
        assets("/fishery/images/bantuan3.png", "/fishery/images/bantuan3.png");
        assets("/fishery/images/bantuan4.png", "/fishery/images/bantuan4.png");
        assets("/fishery/images/bantuan5.png", "/fishery/images/bantuan5.png");
        assets("/fishery/images/bantuan6.png", "/fishery/images/bantuan6.png");
        assets("/fishery/images/bantuan7.png", "/fishery/images/bantuan7.png");

    }
    
}