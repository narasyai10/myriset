/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.api;

import com.myriset.object.DaerahPerikanan;
import com.myriset.repo.AdminAccountRepository;
import com.myriset.repo.DaerahPerikananRepository;
import com.myriset.repo.FeedbackRepository;
import com.myriset.repo.IkanRepository;
import com.myriset.repo.SaranaRepository;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.jooby.Err;
import org.jooby.FlashScope;
import org.jooby.Jooby;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

/**
 *
 * @author Narasyai10
 */
public class DaerahPerikananApi extends Jooby {
    {
        
        /**
         *
         * Everything about your Pets.
         */
        path("/api", () -> {
            /**
            *
            * List pets ordered by id.
            *
            * @param start Start offset, useful for paging. Default is <code>0</code>.
            * @param max Max page size, useful for paging. Default is <code>50</code>.
            * @return Pets ordered by name.
            */
            get("/getDaerahPerikananAll", req -> {
                DaerahPerikananRepository db = require(DaerahPerikananRepository.class);

                int start = 0;
                int max = 50;
                
                return db.list(start, max);
            });

            get("/getDaerahPerikanan", req -> {
                DaerahPerikananRepository db = require(DaerahPerikananRepository.class);

                int halaman = req.param("halaman").intValue(1);
                int start = (halaman-1)*8;
                int max = 8;
                
                return db.list(start, max);
            });

            get("/filterDaerahPerikanan", req -> {
                DaerahPerikananRepository db = require(DaerahPerikananRepository.class);

                String bulan = req.param("bulan").value();
                String tahun = req.param("tahun").value();
                String idLing = req.param("id_ling").value();
                
                return db.filter(bulan, tahun, idLing);
            });

            get("/filterDaerahPerikananPotensi", req -> {
                DaerahPerikananRepository db = require(DaerahPerikananRepository.class);

                String bulan = req.param("bulan").value();
                String tahun = req.param("tahun").value();
                
                return db.filterPotensi(bulan, tahun);
            });

            /**
             *
             * Find pet by ID
             *
             * @param id Pet ID.
             * @return Returns <code>200</code> with a single pet or <code>404</code>
             */
            get("/getDaerahPerikanan/:id", req -> {
                DaerahPerikananRepository db = require(DaerahPerikananRepository.class);

                int id = req.param("id").intValue();

                DaerahPerikanan daerahPerikanan = db.findById(id);
                if (daerahPerikanan == null) {
                    throw new Err(Status.NOT_FOUND);
                }
                return daerahPerikanan;
            });

            post(req -> {
                DaerahPerikananRepository db = require(DaerahPerikananRepository.class);
                DaerahPerikanan daerahPerikanan = req.body(DaerahPerikanan.class);

                int id;
                boolean toReturn = false;

                id = db.insert(daerahPerikanan);

                if (id > 0) {
                    toReturn = true;
                }

                return toReturn;
            });
        });

        path("/api/max-hal/totalDaerah", () -> {
            /**
             * Mendapatkan maksimal halaman Features.
             */
            get(req -> {
                DaerahPerikananRepository db = require(DaerahPerikananRepository.class);
                double jumlahTotal;

                jumlahTotal = (double) db.totalDaerah();
                
                double halamanTotal = jumlahTotal/8;
            
                if((halamanTotal%1) > 0) {
                    halamanTotal = halamanTotal - (halamanTotal%1) + 1;
                }

                return (int) halamanTotal;
            });
        });
    }
    
}
