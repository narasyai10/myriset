/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.repo;

import com.myriset.object.Ekonomi;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

/**
 *
 * @author Narasyai10
 */
/**
 * Database access for Ekonomi.
 */
@RegisterRowMapper(Ekonomi.Mapper.class)
public interface EkonomiRepository {

    /**
    * Insert a Ekonomi and returns the generated PK.
    *
    * @param Ekonomi Ikan to insert.
    * @return Primary key.
    */
    @SqlUpdate("insert into tb_ekonomi (nama_kab, nilai_produksi, tahun) values(:namaKab, :nilaiProduksi, :tahun)")
    @GetGeneratedKeys
    int insert(@BindBean Ekonomi Ekonomi);

    /**
        * List Ekonomi using start/max limits.
    *
    * @param start Start offset.
    * @param max Max number of rows.
    * @return Available Ekonomi.
    */
    @SqlQuery("SELECT id_ekonomi, nama_kab, nilai_produksi, tahun FROM tb_ekonomi ORDER BY nama_kab ASC, tahun ASC LIMIT :max OFFSET :start")
    List<Ekonomi> list(int start, int max);

    /**
    * Find a Ekonomi by ID.
    *
    * @param id Ekonomi ID.
    * @return Ekonomi or null.
   */
    @SqlQuery("SELECT id_ekonomi, nama_kab, nilai_produksi, tahun FROM tb_ekonomi WHERE id_ekonomi=:idEkonomi ORDER BY id_ekonomi DESC")
    Ekonomi findById(int idEkonomi);

    /**
     * Meng-update Jenis Ekonomi berdasarkan ID-nya dan me-return keberhasilannya.
     */
    @SqlUpdate("UPDATE tb_ekonomi SET nama_kab=:namaKab, nilai_produksi=:nilaiProduksi, tahun=:tahun WHERE id_ekonomi=:idEkonomi")
    boolean update(@BindBean Ekonomi Ekonomi, int idEkonomi);

    /**
     * Menghapus Ikan berdasar ID-nya dan me-return keberhasilannya.
     */
    @SqlUpdate("DELETE FROM tb_ekonomi WHERE id_ekonomi=:idEkonomi")
    boolean delete(int idEkonomi);

    /**
     * Cek keberadaan Ikan dengan username yang sama.
     */
   @SqlQuery("SELECT COUNT(*) FROM tb_ekonomi")
   int totalEkonomi();
}
