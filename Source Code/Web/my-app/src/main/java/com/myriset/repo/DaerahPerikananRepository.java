/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.repo;

import com.myriset.object.DaerahPerikanan;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 *
 * @author Narasyai10
 */
/**
 * Database access for DaerahPerikanans.
 */
@RegisterRowMapper(DaerahPerikanan.Mapper.class)
public interface DaerahPerikananRepository {
  /**
   * List DaerahPerikanan using start/max limits.
   *
   * @param start Start offset.
   * @param max Max number of rows.
   * @return Available DaerahPerikanan.
   */
  @SqlQuery("SELECT id, ST_AsText(geom), nama, keterangan, bulan, tahun, potensi, id_ling FROM tb_penangkapan_ikan LIMIT :max OFFSET :start")
  List<DaerahPerikanan> list(int start, int max);

  /**
   * List DaerahPerikanan using bulan/tahun limits.
   *
   * @param start Start offset.
   * @param max Max number of rows.
   * @return Available DaerahPerikanan.
   */
  @SqlQuery("SELECT id, ST_AsText(geom), nama, keterangan, bulan, tahun, potensi, id_ling FROM tb_penangkapan_ikan WHERE bulan=:bulan AND tahun=:tahun AND id_ling=:idLing")
  List<DaerahPerikanan> filter(String bulan, String tahun, String idLing);

  /**
   * List DaerahPerikanan using bulan/tahun limits.
   *
   * @param start Start offset.
   * @param max Max number of rows.
   * @return Available DaerahPerikanan.
   */
   @SqlQuery("SELECT id, ST_AsText(geom), nama, keterangan, bulan, tahun, potensi, id_ling FROM tb_penangkapan_ikan WHERE bulan=:bulan AND tahun=:tahun")
  List<DaerahPerikanan> filterPotensi(String bulan, String tahun);

  /**
   * Find a DaerahPerikanan by ID.
   *
   * @param id DaerahPerikanan ID.
   * @return DaerahPerikanan or null.
   */
  @SqlQuery("SELECT id, ST_AsText(geom), nama, keterangan, bulan, tahun, potensi FROM tb_daerah_penangkapan WHERE id=:id ORDER BY id DESC")
  DaerahPerikanan findById(int id);
 
  /**
    * Insert a DaerahPerikanan and returns the generated PK.
    *
    * @param DaerahPerikanan DaerahPerikanan to insert.
    * @return Primary key.
    */
    @SqlUpdate("insert into tb_daerah_penangkapan (nama_ikan) values(:nama)")
    @GetGeneratedKeys
    int insert(@BindBean DaerahPerikanan DaerahPerikanan);

  /**
    * Update a DaerahPerikanan and returns true if the DaerahPerikanans was updated.
    *
    * @param DaerahPerikanan DaerahPerikanan to update.
    * @return True if the DaerahPerikanan was updated.
    */
    @SqlUpdate("update tb_daerah_penangkapan set name=:name where id=:id")
    boolean update(@BindBean DaerahPerikanan DaerahPerikanan);

    /**
   * Cek keberadaan Ikan dengan username yang sama.
   */
   @SqlQuery("SELECT COUNT(*) FROM tb_daerah_penangkapan")
   int totalDaerah();
}