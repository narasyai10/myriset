/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.repo;

import com.myriset.object.NamaKab;
import java.util.List;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

/**
 *
 * @author Narasyai10
 */
/**
 * Database access for Ikan.
 */
@RegisterRowMapper(NamaKab.Mapper.class)
public interface NamaKabRepository {
    /**
        * List Kabupaten using start/max limits.
   	*
   	* @param start Start offset.
   	* @param max Max number of rows.
   	* @return Available Kabupaten.
    */
    @SqlQuery("SELECT id_kab, nama_kab FROM tb_kabupaten ORDER BY nama_kab ASC LIMIT :max OFFSET :start")
    List<NamaKab> list(int start, int max);
}
