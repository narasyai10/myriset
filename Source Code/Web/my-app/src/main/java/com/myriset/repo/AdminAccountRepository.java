/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.repo;

import com.myriset.object.AdminAccount;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

/**
 *
 * @author Narasyai10
 */
/**
 * Database access for Login.
 */
@RegisterRowMapper(AdminAccount.Mapper.class)
public interface AdminAccountRepository {
    /**
   * Find a Admin by username.
   *
   * @param id Admin username.
   * @return DaerahPerikanan or null.
   */
  @SqlQuery("select password from admin where username=:username")
  String findByUser(String username);
  
  @SqlQuery("select * from admin where password=:password")
  AdminAccount findByPass(String password);
  
  @SqlUpdate("insert into admin (nama_admin, username, password) values(:namaAdmin, :username, :password)")
  @GetGeneratedKeys
  int insert(@BindBean AdminAccount Login);
  
  /**
   * Cek keberadaan username dengan username yang sama.
   */
 @SqlQuery("SELECT COUNT(*) FROM admin WHERE username=:username")
 int isUsernameTaken(String username);
}