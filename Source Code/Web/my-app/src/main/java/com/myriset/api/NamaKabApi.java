/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.api;

import com.myriset.repo.NamaKabRepository;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.jooby.Err;
import org.jooby.FlashScope;
import org.jooby.Jooby;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

/**
 *
 * @author Narasyai10
 */
public class NamaKabApi extends Jooby{
    {
        
        /**
         *
         * Everything about your Pets.
         */
        path("/api", () -> {
            get("/getNamaKab", req -> {
                NamaKabRepository db = require(NamaKabRepository.class);

                int start = 0;
                int max = 20;
                
                return db.list(start, max);
            });
        });
                
                
    }
}
