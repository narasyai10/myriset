/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.repo;

import com.myriset.object.Sarana;
import java.util.List;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

/**
 *
 * @author Narasyai10
 */
/**
 * Database access for Ikan.
 */
@RegisterRowMapper(Sarana.Mapper.class)
public interface SaranaRepository {
    /**
    * Insert a Sarana and returns the generated PK.
    *
    * @param Sarana Ikan to insert.
    * @return Primary key.
    */
    @SqlUpdate("insert into tb_sarana (nama_sarana, jenis_sarana, url_gambar, keterangan) values(:namaSarana, :jenisSarana, :urlGambar, :keterangan)")
    @GetGeneratedKeys
    int insert(@BindBean Sarana Sarana);

    /**
        * List Sarana using start/max limits.
   	*
   	* @param start Start offset.
   	* @param max Max number of rows.
   	* @return Available Sarana.
    */
    @SqlQuery("SELECT id_sarana, nama_sarana, jenis_sarana, url_gambar, keterangan FROM tb_sarana ORDER BY nama_sarana ASC LIMIT :max OFFSET :start")
    List<Sarana> list(int start, int max);

    /**
   	* Find a Sarana by ID.
   	*
   	* @param id Sarana ID.
   	* @return Sarana or null.
   */
    @SqlQuery("SELECT id_sarana, nama_sarana, jenis_sarana, url_gambar, keterangan FROM tb_sarana WHERE id_sarana=:idSarana ORDER BY id_sarana DESC")
    Sarana findById(int idSarana);

    /**
     * Meng-update Jenis Sarana berdasarkan ID-nya dan me-return keberhasilannya.
     */
    @SqlUpdate("UPDATE tb_sarana SET nama_sarana=:namaSarana, jenis_sarana=:jenisSarana, url_gambar=:urlGambar, keterangan=:keterangan WHERE id_sarana=:idSarana")
    boolean update(@BindBean Sarana Sarana, int idSarana);

    /**
     * Menghapus Ikan berdasar ID-nya dan me-return keberhasilannya.
     */
    @SqlUpdate("DELETE FROM tb_sarana WHERE id_sarana=:idSarana")
    boolean delete(int idSarana);

    /**
     * Cek keberadaan Ikan dengan username yang sama.
     */
   @SqlQuery("SELECT COUNT(*) FROM tb_sarana")
   int totalSarana();
}
