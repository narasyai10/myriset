/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.object;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

/**
 *
 * @author Narasyai10
 */
public class NamaKab {
    public static class Mapper implements RowMapper<NamaKab> {
        @Override public NamaKab map(final ResultSet rs, final StatementContext ctx) throws SQLException {
          return new NamaKab(rs.getInt("id_kab"), rs.getString("nama_kab"));
        }
    }
    
    private int idKab;
    private String namaKab;

    public NamaKab(int idKab, String namaKab) {
        this.idKab = idKab;
        this.namaKab = namaKab;
    }

    public int getIdKab() {
        return idKab;
    }

    public String getNamaKab() {
        return namaKab;
    }
    
}
