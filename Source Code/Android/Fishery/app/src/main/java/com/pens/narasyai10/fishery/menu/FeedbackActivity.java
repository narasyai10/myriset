package com.pens.narasyai10.fishery.menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pens.narasyai10.fishery.R;
import com.pens.narasyai10.fishery.api.ApiService;
import com.pens.narasyai10.fishery.api.UtilsApi;
import com.pens.narasyai10.fishery.model.FeedbackBody;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class FeedbackActivity extends AppCompatActivity {
    private EditText inNama;
    private EditText inEmail;
    private EditText inFeedback;
    private Spinner inSubjek;

    private Button btnKirim;

    ProgressDialog loading;

    Context mContext;
    ApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        setTitle("Saran");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = this;
        mApiService = UtilsApi.getAPIService();

        inNama = (EditText) findViewById(R.id.nama);
        inEmail = (EditText) findViewById(R.id.email);
        inSubjek = (Spinner) findViewById(R.id.subjek);
        inFeedback = (EditText) findViewById(R.id.feedback);

        btnKirim = (Button) findViewById(R.id.btnKirim);

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String nama = inNama.getText().toString().trim();
                final String email = inEmail.getText().toString().trim();
                final String subjek = inSubjek.getSelectedItem().toString().trim();
                final String feedback = inFeedback.getText().toString().trim();

                // Check for empty data in the form
                if (!nama.isEmpty() && !email.isEmpty() && !feedback.isEmpty()) {
                    // Feedbak user
                    loading = ProgressDialog.show(mContext, null, "Harap Tunggu...", true, false);
                    requestFeedback(nama, email, subjek, feedback);
                    Log.d("feedback",nama + email + subjek + feedback);
                } else if(nama.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please enter your name!", Toast.LENGTH_LONG).show();
                } else if(email.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please enter your email!", Toast.LENGTH_LONG).show();
                } else if(feedback.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please enter your feedback!", Toast.LENGTH_LONG).show();
                }  else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),"Please enter the credentials!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void requestFeedback(final String nama, final String email, final String subjek, final String feedback) {
        FeedbackBody body = new FeedbackBody(nama ,email, subjek, feedback);
        mApiService.feedbackRequest("application/json", body)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                        if (response.isSuccessful()){
                            Toast.makeText(mContext, "Berhasil", Toast.LENGTH_SHORT).show();

                            inNama.setText(null);
                            inNama.setHint("Nama Kamu");
                            inEmail.setText(null);
                            inEmail.setHint("Email Kamu");
                            inFeedback.setText(null);
                            inFeedback.setHint("Feedback Kamu");

                        } else {
                            Toast.makeText(mContext, "Error : " + response.code(), Toast.LENGTH_SHORT).show();
                        }
                        loading.dismiss();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.e("debug", "onFailure: ERROR > " + t.getMessage());
                        loading.dismiss();
                        Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}