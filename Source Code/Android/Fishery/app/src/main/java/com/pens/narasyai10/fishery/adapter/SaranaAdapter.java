package com.pens.narasyai10.fishery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pens.narasyai10.fishery.R;
import com.pens.narasyai10.fishery.model.IkanBody;
import com.pens.narasyai10.fishery.model.SaranaBody;

import java.util.List;

public class SaranaAdapter extends RecyclerView.Adapter<SaranaAdapter.ViewHolder> {
    private List<SaranaBody> saranaList;
    private Context context;


    public SaranaAdapter(Context context, List<SaranaBody> saranaList) {
        this.saranaList = saranaList;
        this.context = context;
    }

    @Override
    public SaranaAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_items_sarana, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SaranaAdapter.ViewHolder viewHolder, int i) {

        viewHolder.namaSarana.setText(saranaList.get(i).getNamaSarana());
        Glide.with(context).load(saranaList.get(i).getUrlGambar())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        viewHolder.progress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        viewHolder.progress.setVisibility(View.GONE);
                        return false;
                    }
                })
                .override(600,200)
                .thumbnail(0.5f)
                .crossFade()
                .into(viewHolder.imgUrl);
        viewHolder.jenisSarana.setText(saranaList.get(i).getJenisSarana());
    }

    @Override
    public int getItemCount() {
        return saranaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView namaSarana;
        private ImageView imgUrl;
        private TextView jenisSarana;
        private ProgressBar progress;

        public ViewHolder(View view) {
            super(view);

            namaSarana = (TextView)view.findViewById(R.id.txtNama);
            imgUrl = (ImageView) view.findViewById(R.id.imgUrl);
            jenisSarana = (TextView) view.findViewById(R.id.txtJenis);
            progress = (ProgressBar) view.findViewById(R.id.progressBarSarana);
        }
    }
}