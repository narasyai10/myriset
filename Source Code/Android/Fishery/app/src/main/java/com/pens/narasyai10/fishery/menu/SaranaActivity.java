package com.pens.narasyai10.fishery.menu;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.pens.narasyai10.fishery.R;
import com.pens.narasyai10.fishery.adapter.SaranaAdapter;
import com.pens.narasyai10.fishery.api.ApiService;
import com.pens.narasyai10.fishery.api.UtilsApi;
import com.pens.narasyai10.fishery.listener.RecyclerViewClickListener;
import com.pens.narasyai10.fishery.listener.RecyclerViewTouchListener;
import com.pens.narasyai10.fishery.model.SaranaBody;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaranaActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SaranaAdapter saranaAdapter;
    List<SaranaBody> saranaList;

    Context mContext;
    ApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sarana);
        setTitle("Sarana Prasarana");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = this;
        mApiService = UtilsApi.getAPIService();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewSarana);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        saranaList = new ArrayList<>();

        recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getApplicationContext(), recyclerView, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                SaranaBody saranaBody = saranaList.get(position);

                // launch new intent instead of loading fragment
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), DetailSaranaActivity.class);
                intent.putExtra("namaSarana", saranaBody.getNamaSarana());
                intent.putExtra("jenisSarana", saranaBody.getJenisSarana());
                intent.putExtra("urlGambar", saranaBody.getUrlGambar());
                intent.putExtra("keterangan", saranaBody.getKeterangan());

                view.getContext().startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        getSarana();
    }


    private void getSarana() {
        mApiService.getSarana().enqueue(new Callback<List<SaranaBody>>() {
            @Override
            public void onResponse(Call<List<SaranaBody>> call, Response<List<SaranaBody>> response) {

                Log.d("Doing", "res code :" + response.code() );
                if (response.isSuccessful()){
                    List<SaranaBody> list = response.body();
                    SaranaBody saranaBody = null;

                    for (int i =0; i<list.size(); i++){
                        saranaBody = new SaranaBody();
                        String namaIkan = list.get(i).getNamaSarana();
                        String musimIkan = list.get(i).getJenisSarana();
                        String urlGambar = list.get(i).getUrlGambar();
                        String keterangan = list.get(i).getKeterangan();

                        Log.d("gambar", urlGambar);

                        saranaBody.setNamaSarana(namaIkan);
                        saranaBody.setUrlGambar(list.get(i).getUrlGambar());
                        saranaBody.setJenisSarana(musimIkan);
                        saranaBody.setKeterangan(keterangan);
                        saranaList.add(saranaBody);
                    }

                    saranaAdapter = new SaranaAdapter(getApplicationContext(), saranaList);
                    recyclerView.setAdapter(saranaAdapter);

                } else {
                    Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<SaranaBody>> call, Throwable t) {
                Log.e("debug", "onFailure: ERROR > " + t.getMessage());
                Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}