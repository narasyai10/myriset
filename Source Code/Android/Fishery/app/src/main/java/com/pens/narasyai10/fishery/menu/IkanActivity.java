package com.pens.narasyai10.fishery.menu;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.pens.narasyai10.fishery.R;
import com.pens.narasyai10.fishery.adapter.IkanAdapter;
import com.pens.narasyai10.fishery.api.ApiService;
import com.pens.narasyai10.fishery.api.UtilsApi;
import com.pens.narasyai10.fishery.listener.RecyclerViewClickListener;
import com.pens.narasyai10.fishery.listener.RecyclerViewTouchListener;
import com.pens.narasyai10.fishery.model.IkanBody;
import com.pens.narasyai10.fishery.model.SaranaBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IkanActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    IkanAdapter ikanAdapter;
    List<IkanBody> ikanList;

    Context mContext;
    ApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ikan);
        setTitle("Sumberdaya Perikanan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = this;
        mApiService = UtilsApi.getAPIService();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewIkan);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        ikanList = new ArrayList<>();


        recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getApplicationContext(), recyclerView, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                IkanBody ikanBody = ikanList.get(position);

                // launch new intent instead of loading fragment
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), DetailIkanActivity.class);
                intent.putExtra("namaIkan", ikanBody.getNamaIkan());
                intent.putExtra("musimIkan", ikanBody.getMusimIkan());
                intent.putExtra("perairan", ikanBody.getPerairan());
                intent.putExtra("urlGambar", ikanBody.getUrlGambar());
                intent.putExtra("keterangan", ikanBody.getKeterangan());

                view.getContext().startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        getIkan();
    }

    private void getIkan() {
        mApiService.getIkan().enqueue(new Callback<List<IkanBody>>() {
            @Override
            public void onResponse(Call<List<IkanBody>> call, Response<List<IkanBody>> response) {

                Log.d("Doing", "res code :" + response.code() );
                if (response.isSuccessful()){
                    List<IkanBody> list = response.body();
                    IkanBody ikanBody = null;

                    for (int i =0; i<list.size(); i++){
                        ikanBody = new IkanBody();
                        String namaIkan = list.get(i).getNamaIkan();
                        String musimIkan = list.get(i).getMusimIkan();
                        String perairan = list.get(i).getPerairan();
                        String urlGambar = list.get(i).getUrlGambar();
                        String keterangan = list.get(i).getKeterangan();

                        Log.d("debug", namaIkan);

                        ikanBody.setNamaIkan(namaIkan);
                        ikanBody.setMusimIkan(musimIkan);
                        ikanBody.setPerairan(perairan);
                        ikanBody.setUrlGambar(urlGambar);
                        ikanBody.setKeterangan(keterangan);

                        ikanList.add(ikanBody);
                    }

                    ikanAdapter = new IkanAdapter(getApplicationContext(), ikanList);
                    recyclerView.setAdapter(ikanAdapter);

                } else {
                    Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<List<IkanBody>> call, Throwable t) {
                Log.e("debug", "onFailure: ERROR > " + t.getMessage());
                Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}