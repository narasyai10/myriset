package com.pens.narasyai10.fishery.menu;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.pens.narasyai10.fishery.R;
import com.pens.narasyai10.fishery.api.ApiService;
import com.pens.narasyai10.fishery.api.UtilsApi;
import com.pens.narasyai10.fishery.model.EkonomiBody;
import com.pens.narasyai10.fishery.model.Response.ResponseEkonomi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EkonomiActivity extends AppCompatActivity {
    private BarChart chart;
    float barWidth = 0.3f;
    float barSpace = 0f;
    float groupSpace = 0.2f;

    Context mContext;
    ApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ekonomi);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Ekonomi");

        mContext = this;
        mApiService = UtilsApi.getAPIService();

        chart = (BarChart)findViewById(R.id.barChart);
        chart.setDescription(null);
        chart.setPinchZoom(true);
        chart.setScaleEnabled(true);
        chart.setDrawBarShadow(false);
        chart.setDrawGridBackground(false);
        chart.animateXY(2000, 2000);

        getEkonomi();
    }

    private void getEkonomi() {
        mApiService.getEkonomi().enqueue(new Callback<List<ResponseEkonomi>>() {
            @Override
            public void onResponse(Call<List<ResponseEkonomi>> call, Response<List<ResponseEkonomi>> response) {

                Log.d("Doing", "res code :" + response.code() );
                if (response.isSuccessful()){
                    List<ResponseEkonomi> list = response.body();
                    int groupCount = 10;

                    ArrayList xVals = new ArrayList();

                    ArrayList yVals1 = new ArrayList();
                    ArrayList yVals2 = new ArrayList();
                    ArrayList yVals3 = new ArrayList();

                    boolean first = true;

                    int kota=0;
                    for (int i =0; i<list.size(); i++){
                        String namaKab = list.get(i).namaKab;
                        String nilaiProduksi = list.get(i).nilaiProduksi.replace(',', '.');
                        String tahun = list.get(i).tahun;

//                        if(first){
//                            xVals.add(namaKab);
//                            Log.d("xVals", String.valueOf(xVals));
//                        }else{
//                            if (!(list.get(i).namaKab.equals(list.get(i-1).namaKab))) {
//                                xVals.add(namaKab);
//                                Log.d("xVals", String.valueOf(xVals));
//                            }
//                        }

                        if(i%3==0){
                            kota++;
                            yVals1.add(new BarEntry(kota, Float.parseFloat(nilaiProduksi)));
                        }else if(i%3==1){
                            yVals2.add(new BarEntry(kota,Float.parseFloat(nilaiProduksi)));
                        }else if(i%3==2){
                            xVals.add(String.valueOf(kota));
                            yVals3.add(new BarEntry(kota,Float.parseFloat(nilaiProduksi)));
                        }

                        first = false;
                    }

                    BarDataSet set1, set2, set3;
                    set1 = new BarDataSet(yVals1, "2014");
                    set1.setColor(Color.RED);
                    set2 = new BarDataSet(yVals2, "2015");
                    set2.setColor(Color.BLUE);
                    set3 = new BarDataSet(yVals3, "2016");
                    set3.setColor(Color.GREEN);
                    BarData data = new BarData(set1, set2, set3);
                    data.setValueFormatter(new LargeValueFormatter());
                    chart.setData(data);
                    chart.getBarData().setBarWidth(barWidth);
                    chart.getXAxis().setAxisMinimum(0);
                    chart.getXAxis().setAxisMaximum(0 + chart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
                    chart.groupBars(0, groupSpace, barSpace);
                    chart.getData().setHighlightEnabled(false);
                    chart.invalidate();

                    Legend l = chart.getLegend();
                    l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
                    l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
                    l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
                    l.setDrawInside(true);
                    l.setYOffset(20f);
                    l.setXOffset(0f);
                    l.setYEntrySpace(0f);
                    l.setTextSize(8f);

                    //X-axis
                    XAxis xAxis = chart.getXAxis();
                    xAxis.setGranularity(1f);
                    xAxis.setGranularityEnabled(true);
                    xAxis.setCenterAxisLabels(true);
                    xAxis.setDrawGridLines(false);
                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                    xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
                    xAxis.setAxisMaximum(11);

                    //Y-axis
                    chart.getAxisRight().setEnabled(false);
                    YAxis leftAxis = chart.getAxisLeft();
                    leftAxis.setValueFormatter(new LargeValueFormatter());
                    leftAxis.setDrawGridLines(true);
                    leftAxis.setSpaceTop(35f);
                    leftAxis.setAxisMinimum(0f);

                } else {
                    Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<ResponseEkonomi>> call, Throwable t) {
                Log.e("debug", "onFailure: ERROR > " + t.getMessage());
                Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}