package com.pens.narasyai10.fishery.model;

public class IkanBody {
    private String namaIkan;
    private String musimIkan;
    private String perairan;
    private String urlGambar;
    private String keterangan;

    public String getNamaIkan() {
        return namaIkan;
    }

    public void setNamaIkan(String namaIkan) {
        this.namaIkan = namaIkan;
    }

    public String getMusimIkan() {
        return musimIkan;
    }

    public void setMusimIkan(String musimIkan) {
        this.musimIkan = musimIkan;
    }

    public String getPerairan() {
        return perairan;
    }

    public void setPerairan(String perairan) {
        this.perairan = perairan;
    }

    public String getUrlGambar() {
        return urlGambar;
    }

    public void setUrlGambar(String urlGambar) {
        this.urlGambar = urlGambar;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
