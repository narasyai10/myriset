package com.pens.narasyai10.fishery.menu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pens.narasyai10.fishery.R;

import me.biubiubiu.justifytext.library.JustifyTextView;

public class DetailSaranaActivity extends AppCompatActivity {
    private ImageView imgUrl;
    private TextView txtNama, txtJenis;
    private JustifyTextView txtKeterangan;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_sarana);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String namaSarana = intent.getStringExtra("namaSarana");
        String jenisSarana = intent.getStringExtra("jenisSarana");
        String urlGambar = intent.getStringExtra("urlGambar");
        String keterangan = intent.getStringExtra("keterangan");

        setTitle(namaSarana);
        imgUrl = (ImageView) findViewById(R.id.imgSarana);
        txtNama = (TextView) findViewById(R.id.txtNamaSarana);
        txtJenis = (TextView) findViewById(R.id.txtJenisSarana);
        txtKeterangan = (JustifyTextView) findViewById(R.id.txtKeterangan);
        progressBar = (ProgressBar) findViewById(R.id.progressBarSarana1);

        setTitle(namaSarana);
        Glide.with(this).load(urlGambar)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .override(600,200)
                .thumbnail(0.5f)
                .crossFade()
                .into(imgUrl);
        txtNama.setText(namaSarana);
        txtJenis.setText(jenisSarana);
        txtKeterangan.setText(keterangan);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
