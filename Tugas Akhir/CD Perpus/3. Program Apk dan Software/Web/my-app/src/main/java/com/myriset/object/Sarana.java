/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.object;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

/**
 *
 * @author Narasyai10
 */
public class Sarana {
    public static class Mapper implements RowMapper<Sarana> {
        @Override public Sarana map(final ResultSet rs, final StatementContext ctx) throws SQLException {
          return new Sarana(rs.getInt("id_sarana"), rs.getString("nama_sarana"), rs.getString("jenis_sarana"), rs.getString("url_gambar"), rs.getString("keterangan"));
        }
    }
    
    private int idSarana;
    private String namaSarana;
    private String jenisSarana;
    private String urlGambar;
    private String keterangan;

    public Sarana(int id_sarana, String nama_sarana, String jenis_sarana, String url_gambar, String keterangan) {
        this.idSarana = id_sarana;
        this.namaSarana = nama_sarana;
        this.jenisSarana = jenis_sarana;
        this.urlGambar = url_gambar;
        this.keterangan = keterangan;
    }

    public int getIdSarana() {
        return idSarana;
    }

    public String getNamaSarana() {
        return namaSarana;
    }

    public String getJenisSarana() {
        return jenisSarana;
    }

    public String getUrlGambar() {
        return urlGambar;
    }

    public String getKeterangan() {
        return keterangan;
    }

    
}