/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Narasyai10
 */
public class DaerahPerikanan {
    public static class Mapper implements RowMapper<DaerahPerikanan> {
    @Override public DaerahPerikanan map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new DaerahPerikanan(rs.getInt("id"), rs.getString("st_astext"), rs.getString("nama"), rs.getString("keterangan"), rs.getString("bulan"), rs.getString("tahun"), rs.getString("potensi"), rs.getString("id_ling"));
    }
  }

  private int id;
  private String lokasi;
  private String nama;
  private String keterangan;
  private String bulan;
  private String tahun;
  private String potensi;
  private String idLing;

  public DaerahPerikanan(int id, String st_astext, String nama, String keterangan, String bulan, String tahun, String potensi, String id_ling) {
    this.id = id;
    this.lokasi = st_astext;
    this.nama = nama;
    this.keterangan = keterangan;
    this.bulan = bulan;
    this.tahun = tahun;
    this.potensi = potensi;
    this.idLing = id_ling;
  }

    public int getId() {
        return id;
    }

    public String getLokasi() {
        return lokasi;
    }

    public String getNama() {
        return nama;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public String getBulan() {
        return bulan;
    }

    public String getTahun() {
        return tahun;
    }

    public String getPotensi() {
        return potensi;
    }

    public String getIdLing() {
        return idLing;
    }
}