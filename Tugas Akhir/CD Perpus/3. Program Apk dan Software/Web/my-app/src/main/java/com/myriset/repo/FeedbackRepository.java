/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.repo;

import com.myriset.object.Feedback;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

/**
 *
 * @author Narasyai10
 */
/**
 * Database access for Feedback.
 */
@RegisterRowMapper(Feedback.Mapper.class)
public interface FeedbackRepository {
    /**
    * Insert a Feedback and returns the generated PK.
    *
    * @param Feedback  to insert.
    * @return Primary key.
    */
    @SqlUpdate("insert into tb_feedback (nama, email, subjek, feedback) values(:nama, :email, :subjek, :feedback)")
    @GetGeneratedKeys
    int insert(@BindBean Feedback Feedback);

    /**
        * List Feedback using start/max limits.
   	*
   	* @param start Start offset.
   	* @param max Max number of rows.
   	* @return Available Ikan.
    */
    @SqlQuery("SELECT id_feedback, nama, email, subjek, feedback FROM tb_feedback ORDER BY id_feedback DESC LIMIT :max OFFSET :start")
    List<Feedback> list(int start, int max);

    /**
   	* Find a Feedback by ID.
   	*
   	* @param id Feedback ID.
   	* @return Feedback or null.
   */
  @SqlQuery("SELECT id_feedback,  nama, email, subjek, feedback FROM tb_feedback WHERE id_feedback=:idFeedback ORDER BY id_feedback DESC")
  Feedback findById(int idFeedback);

  /**
   * Menghapus Ikan berdasar ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("DELETE FROM tb_feedback WHERE id_feedback=:idFeedback")
  boolean delete(int idFeedback);

  /**
   * Cek keberadaan Ikan dengan username yang sama.
   */
 @SqlQuery("SELECT COUNT(*) FROM tb_feedback")
 int totalFeedback();
}