/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Narasyai10
 */
public class Feedback {
    public static class Mapper implements RowMapper<Feedback> {
        @Override public Feedback map(final ResultSet rs, final StatementContext ctx) throws SQLException {
          return new Feedback(rs.getInt("id_feedback"), rs.getString("nama"), rs.getString("email"), rs.getString("subjek"), rs.getString("feedback"));
        }
    }
    
    private int idFeedback;
    private String nama;
    private String email;
    private String subjek;
    private String feedback;
    
    public Feedback(int id_feedback, String nama, String email, String subjek, String feedback) {
        this.idFeedback = id_feedback;
        this.nama = nama;
        this.email = email;
        this.subjek = subjek;
        this.feedback = feedback;
    }

    public int getIdFeedback() {
        return idFeedback;
    }

    public String getNama() {
        return nama;
    }

    public String getEmail() {
        return email;
    }

    public String getSubjek() {
        return subjek;
    }

    public String getFeedback() {
        return feedback;
    }
    
}