/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Narasyai10
 */
public class Ikan {
    public static class Mapper implements RowMapper<Ikan> {
        @Override public Ikan map(final ResultSet rs, final StatementContext ctx) throws SQLException {
          return new Ikan(rs.getInt("id"), rs.getString("nama_ikan"), rs.getString("musim_ikan"), rs.getString("perairan"), rs.getString("url_gambar"), rs.getString("keterangan"));
        }
    }

    private int id;
    private String namaIkan;
    private String musimIkan;
    private String perairan;
    private String urlGambar;
    private String keterangan;

    public Ikan(int id, String nama_ikan, String musim_ikan, String perairan, String url_gambar, String keterangan) {
        this.id = id;
        this.namaIkan = nama_ikan;
        this.musimIkan = musim_ikan;
        this.perairan = perairan;
        this.urlGambar = url_gambar;
        this.keterangan = keterangan;
    }

    public int getId() {
        return id;
    }

    public String getNamaIkan() {
        return namaIkan;
    }

    public String getMusimIkan() {
        return musimIkan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public String getUrlGambar() {
        return urlGambar;
    }

    public String getPerairan() {
        return perairan;
    }
    
}
