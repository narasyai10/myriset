/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.tools;

import org.jooby.Err;
import org.jooby.FlashScope;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.Session;

/**
 *
 * @author Narasyai10
 */
public class Routes extends Jooby {
    {
	   cookieSession();
	   use(new FlashScope());
        
	   get("/admin/pages/views/sign-in.html", req -> Results.html("/admin/pages/views/sign-in.html"));
        
        get("/admin", req -> {
            Session session = req.session();

            if(session.isSet("ids")) {
                req.set("session", session);

                return Results.redirect("/admin/index.html");
            } else {
                return Results.redirect("/admin/pages/views/sign-in.html");
            }
	   });

        get("/admin/pages/views/sumberdaya.html", req -> {
            Session session = req.session();

            if(session.isSet("ids")) {
                req.set("session", session);

                return Results.redirect("/admin/pages/views/sumberdaya.html");
            } else {
                return Results.redirect("/admin/pages/views/sign-in.html");
            }
       });

        get("/admin/pages/views/sarana.html", req -> {
            Session session = req.session();

            if(session.isSet("ids")) {
                req.set("session", session);

                return Results.redirect("/admin/pages/views/sarana.html");
            } else {
                return Results.redirect("/admin/pages/views/sign-in.html");
            }
       });
        
        get("/admin/pages/views/ekonomi.html", req -> {
            Session session = req.session();

            if(session.isSet("ids")) {
                req.set("session", session);

                return Results.redirect("/admin/pages/views/ekonomi.html");
            } else {
                return Results.redirect("/admin/pages/views/sign-in.html");
            }
       });

        get("/admin/pages/views/feedback.html", req -> {
            Session session = req.session();

            if(session.isSet("ids")) {
                req.set("session", session);

                return Results.redirect("/admin/pages/views/feedback.html");
            } else {
                return Results.redirect("/admin/pages/views/sign-in.html");
            }
       });
    }
    
}