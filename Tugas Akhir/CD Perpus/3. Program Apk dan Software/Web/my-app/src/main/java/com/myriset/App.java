package com.myriset;

import com.myriset.api.*;
import com.myriset.assets.*;
import com.myriset.repo.*;
import com.myriset.tools.Routes;
import com.myriset.view.View;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.jooby.FlashScope;
import org.jooby.Jooby;
import org.jooby.apitool.ApiTool;
import org.jooby.handlers.CorsHandler;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

/**
 * @author jooby generator
 */
public class App extends Jooby {
    
    {   
        use(new FlashScope());
        
        use(new Jackson());

        use(new Jdbc());

        use(new Jdbi3()
            /** Install SqlObjectPlugin */
            .doWith(jdbi -> {
              jdbi.installPlugin(new SqlObjectPlugin());
            })/** Creates a transaction per request and attach PetRepository */
            .transactionPerRequest(new TransactionalRequest()
                    .attach(DaerahPerikananRepository.class)
                    .attach(AdminAccountRepository.class)
                    .attach(FeedbackRepository.class)
                    .attach(IkanRepository.class)
                    .attach(SaranaRepository.class)
                    .attach(EkonomiRepository.class)
                    .attach(NamaKabRepository.class)
                    
            )
        );
        
        use(new View());
        use(new Images());
        use(new Plugins());
        use(new Css());
        use(new Fonts());
        use(new Js());
        use(new Routes());
        
        //Api
        use(new AdminAccountApi());
        use(new DaerahPerikananApi());
        use(new FeedbackApi());
        use(new IkanApi());
        use(new SaranaApi());
        use(new EkonomiApi());
        use(new NamaKabApi());
        
        use(new ApiTool());
    }

    {
        get("/", () -> "Hello World!");
    }
  

    {
        use("*", new CorsHandler());

    }
  
    

  public static void main(final String[] args) {
    run(App::new, args);
  }

}
