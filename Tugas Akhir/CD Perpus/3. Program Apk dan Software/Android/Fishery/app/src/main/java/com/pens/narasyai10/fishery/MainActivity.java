package com.pens.narasyai10.fishery;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.pens.narasyai10.fishery.menu.AboutActivity;
import com.pens.narasyai10.fishery.menu.EkonomiActivity;
import com.pens.narasyai10.fishery.menu.FeedbackActivity;
import com.pens.narasyai10.fishery.menu.IkanActivity;
import com.pens.narasyai10.fishery.menu.MapsActivity;
import com.pens.narasyai10.fishery.menu.SaranaActivity;

public class MainActivity extends AppCompatActivity {

    private View btnMaps, btnIkan, btnSarana, btnEkonomi, btnFeedback, btnAbout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMaps = findViewById(R.id.btnMaps);
        btnIkan = findViewById(R.id.btnIkan);
        btnSarana = findViewById(R.id.btnSarana);
        btnEkonomi = findViewById(R.id.btnEkonomi);
        btnFeedback = findViewById(R.id.btnFeedback);
        btnAbout = findViewById(R.id.btnAbout);

        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(intent);
            }
        });

        btnIkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), IkanActivity.class);
                startActivity(intent);
            }
        });

        btnSarana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SaranaActivity.class);
                startActivity(intent);
            }
        });

        btnEkonomi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EkonomiActivity.class);
                startActivity(intent);
            }
        });

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FeedbackActivity.class);
                startActivity(intent);
            }
        });

        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(intent);
            }
        });
    }
}