package com.pens.narasyai10.fishery.menu;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.pens.narasyai10.fishery.R;
import com.pens.narasyai10.fishery.api.ApiService;
import com.pens.narasyai10.fishery.api.UtilsApi;
import com.pens.narasyai10.fishery.model.DaerahIkanBody;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    MarkerOptions markerOptions;
    PolygonOptions polygonOptions;

    private GoogleMap mMap;
    CameraPosition cameraPosition;

    String tepi[];
    LatLng batas;

    Context mContext;
    ApiService mApiService;

    private static final int MY_PERMISSIONS_REQUEST_LOCATIONS = 1;

    LocationManager locationManager ;
    boolean GpsStatus ;

    private String bulan = "Februari";
    private String tahun = "2016";
    private Spinner inBulan;
    private Spinner inTahun;
    private Button btnFilter;

    private PopupWindow window;
    private CheckBox cSalinitas;
    private CheckBox cSuhu;
    private CheckBox cKlorofil;

//    ArrayList<String> years = new ArrayList<String>();
//    int thisYear = Calendar.getInstance().get(Calendar.YEAR);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //checkPermission();
        //checkGpsStatus();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mContext = this;
        mApiService = UtilsApi.getAPIService();

        inBulan = (Spinner) findViewById(R.id.inBulan);
        inTahun = (Spinner) findViewById(R.id.inTahun);
        btnFilter = (Button) findViewById(R.id.btnFilter);

        cSalinitas = (CheckBox) findViewById(R.id.cekSalinitas);
        cSuhu = (CheckBox) findViewById(R.id.cekSuhu);
        cKlorofil = (CheckBox) findViewById(R.id.cekKlorofil);

//        for (int i = 2010; i <= thisYear; i++) {
//            years.add(Integer.toString(i));
//        }
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, years);
//        inTahun.setAdapter(adapter);

        inBulan.setSelection(3);
        inTahun.setSelection(2);

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String bulanBaru = inBulan.getSelectedItem().toString().trim();
                final String tahunBaru = inTahun.getSelectedItem().toString().trim();

                mMap.clear();
                //getMarkersPolygon(bulanBaru, tahunBaru);

            }
        });


    }

    public void itemClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        String str="";
        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.cekBasemap:
                str = checked?"Basemap Selected":"Basemap Deselected";
                if (checked) {
                    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.cekSalinitas:
                str = checked?"Salinitas Selected":"Salinitas Deselected";
                if (checked) {
                    cSalinitas.setChecked(true);
                    getMarkersPolygon(bulan, tahun, "1");
                    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                } else {
                    mMap.clear();
                    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                    if (cKlorofil.isChecked()) {
                        getMarkersPolygon(bulan, tahun, "3");
                    } if (cSuhu.isChecked()) {
                        getMarkersPolygon(bulan, tahun, "3");
                    }
                }
                break;
            case R.id.cekSuhu:
                str = checked?"Suhu Selected":"Suhu Deselected";
                if (checked) {
                    cSuhu.setChecked(true);
                    getMarkersPolygon(bulan, tahun, "1");
                    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                } else {
                    mMap.clear();
                    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                    if (cKlorofil.isChecked()) {
                        getMarkersPolygon(bulan, tahun, "3");
                    } if (cSalinitas.isChecked()) {
                        getMarkersPolygon(bulan, tahun, "1");
                    }
                }
                break;
            case R.id.cekKlorofil:
                str = checked?"Klorofil Selected":"Klorofil Deselected";
                if (checked) {
                    cKlorofil.setChecked(true);
                    getMarkersPolygon(bulan, tahun, "3");
                    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                } else {
                    mMap.clear();
                    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                    if (cSuhu.isChecked()) {
                        getMarkersPolygon(bulan, tahun, "3");
                    } if (cSalinitas.isChecked()) {
                        getMarkersPolygon(bulan, tahun, "1");
                    }
                }
                break;
        }
    }

    private void checkPermission(){
        // Assume thisActivity is the current activity
        int permissionCheck = ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED){
            Toast.makeText(getApplicationContext(),"Permission already granted.",Toast.LENGTH_LONG).show();
        } else {
            requestPermission();
        }
    }

    private void requestPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(MapsActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MapsActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(MapsActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATIONS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void checkGpsStatus(){

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(GpsStatus == true) {
            Toast.makeText(getApplicationContext(),"Location Services Is Enable.",Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(),"Location Services Is Disable.",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);

        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng selatMadura = new LatLng(-7.1277775, 112.6574904);

        mMap.addMarker(new MarkerOptions().position(selatMadura).title("Marker in Selat Madura"));

        // For zooming automatically to the location of the marker
        cameraPosition = new CameraPosition.Builder().target(selatMadura).zoom(9).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        getMarkersPolygon(bulan, tahun, "3");
        getMarkersPolygon(bulan, tahun, "3");
        getMarkersPolygon(bulan, tahun, "1");
    }


    //mengambar polygon
    private void addPolygon(final ArrayList outer, final String warna, final String namaPotensi, final String keterangan, final String bulan, final String tahun, final int id) {
        polygonOptions = new PolygonOptions();

        polygonOptions.addAll(outer);
        polygonOptions.strokeColor(Color.rgb(30, 30, 30));
        polygonOptions.strokeWidth(0.f);
        polygonOptions.fillColor(Color.parseColor(warna));
        polygonOptions.clickable(true);

        Log.d("namaPotensi", namaPotensi);
        DaerahIkanBody info = new DaerahIkanBody();
        info.setNama(namaPotensi);
        info.setKeterangan(keterangan);
        info.setBulan(bulan);
        info.setTahun(tahun);

        mMap.addPolygon(polygonOptions)
                .setTag(info);

        mMap.setOnPolygonClickListener(new GoogleMap.OnPolygonClickListener() {
            @Override
            public void onPolygonClick(Polygon polygon) {
                DaerahIkanBody daerahIkanBody = (DaerahIkanBody) polygon.getTag();

                LayoutInflater inflater = (LayoutInflater) MapsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.custom_info_window, null);


                window = new PopupWindow(layout, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);

                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                window.setOutsideTouchable(true);

                // Get a reference for the custom view close button
                ImageButton closeButton = (ImageButton) layout.findViewById(R.id.ib_close);
                TextView txtNama = (TextView) layout.findViewById(R.id.nama);
                TextView txtKeterangan = (TextView) layout.findViewById(R.id.keterangan);
                TextView txtBulan = (TextView) layout.findViewById(R.id.bulan);
                TextView txtTahun = (TextView) layout.findViewById(R.id.tahun);

                txtNama.setText(daerahIkanBody.getNama());
                txtKeterangan.setText(daerahIkanBody.getKeterangan());
                txtBulan.setText(daerahIkanBody.getBulan());
                txtTahun.setText(daerahIkanBody.getTahun());

                // Set a click listener for the popup window close button
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Dismiss the popup window
                        window.dismiss();

                    }
                });

                window.showAtLocation(layout, Gravity.CENTER, 0, 0);

            }
        });



    }

    // Fungsi get JSON Polygon
    private void getMarkersPolygon(final String bulanBaru, final String tahunBaru, final String idLingBaru) {
        mApiService.getFilterDaerah(bulanBaru, tahunBaru, idLingBaru)
                .enqueue(new Callback<List<DaerahIkanBody>>() {
                    @Override
                     public void onResponse(Call<List<DaerahIkanBody>> call, retrofit2.Response<List<DaerahIkanBody>> response) {

                        Log.d("Doing", "res code :" + response.code() );
                        if (response.isSuccessful()){
                            List<DaerahIkanBody> list = response.body();

                            String nama;
                            String keterangan;
                            String bulan;
                            String tahun;
                            String warna;
                            String potensi;
                            int id;

                            for (int i = 0; i < list.size(); i++){
                                tepi = list.get(i).getLokasi().replace("MULTIPOLYGON(((","").replace(")))","").split(",");
                                potensi = list.get(i).getPotensi();
                                nama = list.get(i).getNama();
                                keterangan = list.get(i).getKeterangan();
                                bulan = list.get(i).getBulan();
                                tahun = list.get(i).getTahun();
                                id = list.get(i).getId();

                                ArrayList outer = new ArrayList<LatLng>();

                                for (String aTepi : tepi) {
                                    Log.d("poli", aTepi);
                                    String[] latlng = aTepi.split(" ");
                                    batas = new LatLng(Double.parseDouble(latlng[1]), Double.parseDouble(latlng[0]));
                                    outer.add(batas);
                                    Log.d("poli", String.valueOf(batas));
                                }

                                final int finalI = i;


                                if (potensi.equals("1")) {
                                    warna = "#FE0000";
                                } else if (potensi.equals("2")) {
                                    warna = "#19BE00";
                                } else {
                                    warna = "#73FA73";
                                }

                                Log.d("namaPotensi", nama);

                                addPolygon(outer, warna, nama, keterangan, bulan, tahun, id);

                            }
                        } else {
                            Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();

                        }
                     }

                     @Override
                     public void onFailure(Call<List<DaerahIkanBody>> call, Throwable t) {
                         Log.e("debug", "onFailure: ERROR > " + t.getMessage());
                         Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();
                     }
                });

    }

}