package com.pens.narasyai10.fishery.model;

public class EkonomiBody {
    private String namaKab;
    private String nilaiProduksi;
    private String tahun;

    public String getNamaKab() {
        return namaKab;
    }

    public void setNamaKab(String namaKab) {
        this.namaKab = namaKab;
    }

    public String getNilaiProduksi() {
        return nilaiProduksi;
    }

    public void setNilaiProduksi(String nilaiProduksi) {
        this.nilaiProduksi = nilaiProduksi;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }
}
