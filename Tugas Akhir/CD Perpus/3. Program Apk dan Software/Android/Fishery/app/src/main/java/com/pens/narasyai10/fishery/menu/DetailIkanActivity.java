package com.pens.narasyai10.fishery.menu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pens.narasyai10.fishery.R;

import me.biubiubiu.justifytext.library.JustifyTextView;

public class DetailIkanActivity extends AppCompatActivity {
    private ImageView imgUrl;
    private TextView txtNama, txtMusim, txtPerairan;
    private JustifyTextView txtKeterangan;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_ikan);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        String namaIkan = intent.getStringExtra("namaIkan");
        String musimIkan = intent.getStringExtra("musimIkan");
        String perairan = intent.getStringExtra("perairan");
        String urlGambar = intent.getStringExtra("urlGambar");
        String keterangan = intent.getStringExtra("keterangan");

        imgUrl = (ImageView) findViewById(R.id.imgIkan);
        txtNama = (TextView) findViewById(R.id.txtNamaIKan);
        txtMusim = (TextView) findViewById(R.id.txtMusimIKan);
        txtPerairan = (TextView) findViewById(R.id.txtPerairan);
        txtKeterangan = (JustifyTextView) findViewById(R.id.txtKeterangan);

        progressBar = (ProgressBar) findViewById(R.id.progressBarIkan1);

        setTitle(namaIkan);
        Glide.with(this).load(urlGambar)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .override(600,200)
                .thumbnail(0.5f)
                .crossFade()
                .into(imgUrl);
        txtNama.setText(namaIkan);
        txtMusim.setText(musimIkan);
        txtPerairan.setText(perairan);
        txtKeterangan.setText(keterangan);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
