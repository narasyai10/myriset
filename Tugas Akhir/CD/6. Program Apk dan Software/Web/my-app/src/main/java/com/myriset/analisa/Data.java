/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.analisa;

/**
 *
 * @author Narasyai10
 */
public class Data {
    private double mX = 0;
    private double mY = 0;
    private double mZ = 0;
    private double mA = 0;
    private int mCluster = 0;

    public Data()
    {
        return;
    }

    public Data(double x, double y, double z, double a)
    {
        this.X(x);
        this.Y(y);
        this.Z(z);
        this.A(a);
        return;
    }

    public void X(double x)
    {
        this.mX = x;
        return;
    }

    public double X()
    {
        return this.mX;
    }

    public void Y(double y)
    {
        this.mY = y;
        return;
    }

    public double Y()
    {
        return this.mY;
    }
    
    public void Z(double z)
    {
        this.mZ = z;
        return;
    }

    public double Z()
    {
        return this.mZ;
    }
    
     public void A(double a)
    {
        this.mA = a;
        return;
    }

    public double A()
    {
        return this.mA;
    }

    public void cluster(int clusterNumber)
    {
        this.mCluster = clusterNumber;
        return;
    }

    public int cluster()
    {
        return this.mCluster;
    }
}