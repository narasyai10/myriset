/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.api;

import com.myriset.object.Sarana;
import com.myriset.repo.SaranaRepository;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.jooby.Err;
import org.jooby.FlashScope;
import org.jooby.Jooby;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

/**
 *
 * @author Narasyai10
 */
public class SaranaApi extends Jooby{
    {
        
        /**
         *
         * Everything about your Pets.
         */
        path("/api", () -> {
            post("/insertSarana", req -> {
                SaranaRepository dbSarana = require(SaranaRepository.class);
                Sarana sarana = req.body(Sarana.class);

                int id;
                boolean toReturn = false;

                id = dbSarana.insert(sarana);

                if (id > 0) {
                    toReturn = true;
                }

                return toReturn;
            });

            get("/getSaranaAll", req -> {
                SaranaRepository db = require(SaranaRepository.class);

                int start = 0;
                int max = 50;
                
                return db.list(start, max);
            });
            

             /**
             * Daftar Sarana dengan jumlah maksimal max data dan dimulai dari start.
             */
            get("/getSarana", req -> {
                SaranaRepository db = require(SaranaRepository.class);

                int halaman = req.param("halaman").intValue(1);
                int start = (halaman-1)*8;
                int max = 8;
                
                return db.list(start, max);
               
            });


            /**
             *
             * Find Sarana by ID
             *
             * @param id Ikan ID.
             * @return Returns <code>200</code> with a single Ikan or <code>404</code>
             */
            get("/getSarana/:id", req -> {
                SaranaRepository dbSarana = require(SaranaRepository.class);

                int id = req.param("id").intValue();

                Sarana sarana = dbSarana.findById(id);
                if (sarana == null) {
                    throw new Err(Status.NOT_FOUND);
                }
                return sarana;
            });
            
            /**
             * Update sarana berdasarkan ID.
             */
            post("/updateSarana/:id", req -> {
                SaranaRepository dbSarana = require(SaranaRepository.class);
                Sarana sarana = req.body(Sarana.class);

                int id = req.param("id").intValue();
                boolean status = false;

                 if (!dbSarana.update(sarana, id)) {
                    throw new Err(Status.NOT_FOUND);
                } else {
                    status = true;
                }

                return status;
            });

            /**
             * Delete Ikan berdasarkan ID.
             */
            delete("/deleteSarana/:id", req -> {
                SaranaRepository db = require(SaranaRepository.class);
                int id = req.param("id").intValue();
                boolean status = false;

                if (!db.delete(id)) {
                    throw new Err(Status.NOT_FOUND);
                } else {
                    status = true;
                }

                return status;
            });
        });

        path("/api/max-hal/totalSarana", () -> {
            /**
             * Mendapatkan maksimal halaman Features.
             */
            get(req -> {
                SaranaRepository db = require(SaranaRepository.class);
                double jumlahTotal;

                jumlahTotal = (double) db.totalSarana();
                
                double halamanTotal = jumlahTotal/8;
            
                if((halamanTotal%1) > 0) {
                    halamanTotal = halamanTotal - (halamanTotal%1) + 1;
                }

                return (int) halamanTotal;
            });
        });
        
    }
}
