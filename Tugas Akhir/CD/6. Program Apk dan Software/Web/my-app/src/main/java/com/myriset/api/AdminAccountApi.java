/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.api;

import com.myriset.object.AdminAccount;
import com.myriset.object.LoginAdmin;
import com.myriset.repo.AdminAccountRepository;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.jooby.FlashScope;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Session;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author Narasyai10
 */
public class AdminAccountApi extends Jooby{
    {
	cookieSession();
        
        /**
         *
         * Everything about your Pets.
         */
        path("/api", () -> {
           
            
            /**
             * Menambahkan User Account dan me-return statusnya.
             */
            post("/daftarAdmin",req -> {
                AdminAccountRepository db = require(AdminAccountRepository.class);
                AdminAccount adminAccount = req.body(AdminAccount.class);

                int id;
                boolean toReturn = false;
                
                if(db.isUsernameTaken(adminAccount.getUsername()) != 1) {
                    adminAccount.setPassword(BCrypt.hashpw(
                        adminAccount.getPassword(),
                        BCrypt.gensalt()
                    ));

                    id = db.insert(adminAccount);

                    if(id > 0) {
                        toReturn = true;
                    }
                }

                return toReturn;
            });

            //~ Login
            post("/loginAdmin", req -> {
                AdminAccountRepository db = require(AdminAccountRepository.class);
                LoginAdmin login = req.form(LoginAdmin.class);

                String username = login.getUsername();
                String password = login.getPassword();

                String realPassword = db.findByUser(username);

                if(realPassword != null) {
                    if(BCrypt.checkpw(password, realPassword)) {
                        AdminAccount user = db.findByPass(realPassword);

                        Session session = req.session();
                
                        session.set("ids", user.getId());
                        session.set("username", user.getUsername());
                        session.set("nama_admin", user.getNamaAdmin());
                        
                        return Results.redirect("/admin");
                    }
                }
                
                req.flash("gagal", "true");

                return Results.redirect("/admin/pages/views/sign-in.html");
            });

            //~ Logout
            get("/logout", req -> {
                Session session = req.session();
            
                if(session.isSet("ids")) {
                    session.unset();
                }
                
                return Results.redirect("/admin");
            });

        });
        
    }
}