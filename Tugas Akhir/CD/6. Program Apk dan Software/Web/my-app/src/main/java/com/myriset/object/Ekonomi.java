/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Narasyai10
 */
public class Ekonomi {
    public static class Mapper implements RowMapper<Ekonomi> {
        @Override public Ekonomi map(final ResultSet rs, final StatementContext ctx) throws SQLException {
          return new Ekonomi(rs.getInt("id_ekonomi"), rs.getString("nama_kab"), rs.getString("nilai_produksi"), rs.getString("tahun"));
        }
    }
    
    private int idEkonomi;
    private String namaKab;
    private String nilaiProduksi;
    private String tahun;

    public Ekonomi(int id_ekonomi, String nama_kab, String nilai_produksi, String tahun) {
        this.idEkonomi = id_ekonomi;
        this.namaKab = nama_kab;
        this.nilaiProduksi = nilai_produksi;
        this.tahun = tahun;
    }

    public int getIdEkonomi() {
        return idEkonomi;
    }

    public String getNamaKab() {
        return namaKab;
    }

    public String getNilaiProduksi() {
        return nilaiProduksi;
    }

    public String getTahun() {
        return tahun;
    }
    
    
    
}
