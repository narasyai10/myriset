/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.object;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

/**
 *
 * @author Narasyai10
 */
public class AdminAccount {
    public static class Mapper implements RowMapper<AdminAccount> {
        @Override public AdminAccount map(final ResultSet rs, final StatementContext ctx) throws SQLException {
          return new AdminAccount(rs.getInt("id_admin"), rs.getString("nama_admin"), rs.getString("username"), rs.getString("password"), rs.getString("create_at"), rs.getString("update_at"));
        }
    }

    private int id;
    private String namaAdmin;
    private String username;
    private String password;
    private String createAt;
    private String updateAt;

    public AdminAccount(int id_admin, String nama_admin, String username, String password, String create_at, String update_at) {
        this.id = id_admin;
        this.namaAdmin = nama_admin;
        this.username = username;
        this.password = password;
        this.createAt = create_at;
        this.updateAt = update_at;
    }

    public int getId() {
        return id;
    }

    public String getNamaAdmin() {
        return namaAdmin;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreateAt() {
        return createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

}
