/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.assets;

import org.jooby.Jooby;

/**
 *
 * @author Narasyai10
 */
public class Fonts extends Jooby {
	{
        assets("/fishery/fonts/fontawesome-webfont.ttf", "/fishery/fonts/fontawesome-webfont.ttf");
        assets("/fishery/fonts/fontawesome-webfont.woff", "/fishery/fonts/fontawesome-webfont.woff");
        assets("/fishery/fonts/fontawesome-webfont.woff2", "/fishery/fonts/fontawesome-webfont.woff2");

	}
    
}
