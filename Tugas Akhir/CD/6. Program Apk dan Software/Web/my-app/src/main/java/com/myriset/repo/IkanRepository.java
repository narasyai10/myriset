/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.repo;

import com.myriset.object.Ikan;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

/**
 *
 * @author Narasyai10
 */
/**
 * Database access for Ikan.
 */
@RegisterRowMapper(Ikan.Mapper.class)
public interface IkanRepository {
    /**
    * Insert a Ikan and returns the generated PK.
    *
    * @param Ikan Ikan to insert.
    * @return Primary key.
    */
    @SqlUpdate("insert into tb_ikan (nama_ikan, musim_ikan, perairan, url_gambar, keterangan) values(:namaIkan, :musimIkan, :perairan, :urlGambar, :keterangan)")
    @GetGeneratedKeys
    int insert(@BindBean Ikan Ikan);

    /**
        * List Ikan using start/max limits.
   	*
   	* @param start Start offset.
   	* @param max Max number of rows.
   	* @return Available Ikan.
    */
    @SqlQuery("SELECT id, nama_ikan, musim_ikan, perairan, url_gambar, keterangan FROM tb_ikan ORDER BY nama_ikan ASC LIMIT :max OFFSET :start")
    List<Ikan> list(int start, int max);

    /**
   	* Find a Ikan by ID.
   	*
   	* @param id Ikan ID.
   	* @return Ikan or null.
   */
  @SqlQuery("SELECT id,  nama_ikan, musim_ikan, perairan, url_gambar, keterangan FROM tb_ikan WHERE id=:id ORDER BY id DESC")
  Ikan findById(int id);

  /**
   * Meng-update Jenis Ikan berdasarkan ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("UPDATE tb_ikan SET nama_ikan=:namaIkan, musim_ikan=:musimIkan, perairan=:perairan, url_gambar=:urlGambar, keterangan=:keterangan WHERE id=:id")
  boolean update(@BindBean Ikan Ikan, int id);

  /**
   * Menghapus Ikan berdasar ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("DELETE FROM tb_ikan WHERE id=:id")
  boolean delete(int id);

  /**
   * Cek keberadaan Ikan dengan username yang sama.
   */
 @SqlQuery("SELECT COUNT(*) FROM tb_ikan")
 int totalIkan();
}