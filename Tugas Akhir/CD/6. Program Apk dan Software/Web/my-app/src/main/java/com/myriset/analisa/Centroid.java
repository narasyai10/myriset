/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.analisa;

/**
 *
 * @author Narasyai10
 */

public class Centroid {
    private double mX = 0.0;
    private double mY = 0.0;
    private double mZ = 0.0;
    private double mA = 0.0;

    public Centroid()
    {
        return;
    }

    public Centroid( double newX, double newY, double newZ, double newA)
    {
        this.mX = newX;
        this.mY = newY;
        this.mZ = newZ;
        this.mA = newA;
        return;
    }

    public void X(double newX)
    {
        this.mX = newX;
        return;
    }

    public double X()
    {
        return this.mX;
    }

    public void Y(double newY)
    {
        this.mY = newY;
        return;
    }

    public double Y()
    {
        return this.mY;
    }
    
    public void Z(double newZ)
    {
        this.mZ = newZ;
        return;
    }

    public double Z()
    {
        return this.mZ;
    }
    
    public void A(double newA)
    {
        this.mA = newA;
        return;
    }

    public double A()
    {
        return this.mA;
    }
}