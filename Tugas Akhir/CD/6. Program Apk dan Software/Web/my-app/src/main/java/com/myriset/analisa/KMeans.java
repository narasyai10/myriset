/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.analisa;

/**
 *
 * @author Narasyai10
 */

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.jooby.Err;
import org.jooby.FlashScope;
import org.jooby.Jooby;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

public class KMeans extends Jooby {   
    //1 Menentukan jumlah cluster
    private static final int NUM_CLUSTERS = 3;    // Total clusters.
    private static final int TOTAL_DATA = 9;      // Total data points.
    //Inisialisasi Sample
    private static final double SAMPLES[][] = new double[][] {{ 44.74, 27.68, 1625.5, 1}, 
                                                                {37.2, 26.72, 235.53, 2}, 
                                                                { 45.1, 27.67, 1316.23, 4}, 
                                                                { 45.68, 28.82, 1304.02, 5}, 
                                                                { 43.56, 28.87, 783.77, 6}, 
                                                                { 41.82, 25.0, 480.65, 7},  
                                                                {44.2, 26.5, 409.63, 8}, 
                                                                { 51.94, 26.5, 81.3, 11},
                                                                { 49.64, 26.5, 11.66, 12}};
    
//    private static final double SAMPLES[][] = new double[][] {{ 14, 1, 3.16}, 
//                                                                {1, 5, 3.22}, 
//                                                                { 4, 2, 3.29}, 
//                                                                { 2, 1, 2.83}, 
//                                                                { 3, 1, 3.15}};
    
    private static final double[] Y ={29.0, 27.8, 29.1, 29.3, 28.4, 28.1, 28.2, 30.5, 28.9};
    
    private static final DecimalFormat df = new DecimalFormat("#.####");
    
    private static ArrayList<Data> dataSet = new ArrayList<Data>();
    private static ArrayList<Centroid> centroids = new ArrayList<Centroid>();
   
    private static void getSample()
    {
        System.out.println("Inisialisasi Data Sample");
        //A Suhu
        //B Salinity
        //C Klorofil
        System.out.println("No   A       B       C        Bulan");
        for(int i=0; i< SAMPLES.length; i++) {
            System.out.print(i+1 +"   ");
            for(int j=0; j<SAMPLES[i].length; j++) {
                System.out.print(SAMPLES[i][j] + "   ");
            }
            System.out.println("");
        }
        
    }
    
    //Menentukan centroid setiap cluster secara acak.

    private static void initialize()
    {
        System.out.println("\nInisialisasi centroid :");
        centroids.add(new Centroid(44.74, 27.68, 1625.5, 1)); // lowest set.
        centroids.add(new Centroid(44.2, 26.5, 409.63, 8)); // highest set.
        centroids.add(new Centroid(49.64, 26.5, 11.66, 12)); // highest set.
//        centroids.add(new Centroid(9, 3, 2.94)); // lowest set.
//        centroids.add(new Centroid(1, 1, 3.18)); // highest set.
//        centroids.add(new Centroid(1, 2, 3.15)); // highest set.
        System.out.println("     (" + centroids.get(0).X() + ", " + centroids.get(0).Y() + ", " + centroids.get(0).Z() + ", " + centroids.get(0).A() + ")");
        System.out.println("     (" + centroids.get(1).X() + ", " + centroids.get(1).Y() + ", " + centroids.get(1).Z() + ", " + centroids.get(1).A() + ")");
        System.out.println("     (" + centroids.get(2).X() + ", " + centroids.get(2).Y() + ", " + centroids.get(2).Z() + ", " + centroids.get(2).A() + ")");
        System.out.print("\n");
        return;
    }
   
     private static void kMeanCluster()
    {
        final double bigNumber = Math.pow(10, 10);    // some big number that's sure to be larger than our data range.
        double minimum = bigNumber;                   // The minimum value to beat. 
        double distance = 0.0;                        // The current minimum value.
        int sampleNumber = 0;
        int cluster = 0;
        boolean isStillMoving = true;
        Data newData = null;
        
        // Add in new data, one at a time, recalculating centroids with each new one. 
        while(dataSet.size() < TOTAL_DATA)
        {
            newData = new Data(SAMPLES[sampleNumber][0], SAMPLES[sampleNumber][1], SAMPLES[sampleNumber][2], SAMPLES[sampleNumber][3]);
            dataSet.add(newData);
            
            minimum = bigNumber;
            for(int i = 0; i < NUM_CLUSTERS; i++)
            {   
                distance = dist(newData, centroids.get(i));
                if(distance < minimum){
                    minimum = distance;
                    cluster = i;
                }
            }
            newData.cluster(cluster);
            
            // calculate new centroids.
            for(int i = 0; i < NUM_CLUSTERS; i++)
            {
                int totalX = 0;
                int totalY = 0;
                int totalZ = 0;
                int totalA = 0;
                int totalInCluster = 0;
                for(int j = 0; j < dataSet.size(); j++)
                {
                    if(dataSet.get(j).cluster() == i){
                        totalX += dataSet.get(j).X();
                        totalY += dataSet.get(j).Y();
                        totalZ += dataSet.get(j).Z();
                        totalA += dataSet.get(j).A();
                        totalInCluster++;
                    }
                }
                if(totalInCluster > 0){
                    centroids.get(i).X(totalX / totalInCluster);
                    centroids.get(i).Y(totalY / totalInCluster);
                    centroids.get(i).Z(totalZ / totalInCluster);
                    centroids.get(i).A(totalA / totalInCluster);
                }
            }
            sampleNumber++;
            
        }
        
        // Now, keep shifting centroids until equilibrium occurs.
        while(isStillMoving)
        {
            // calculate new centroids.
            for(int i = 0; i < NUM_CLUSTERS; i++)
            {
                int totalX = 0;
                int totalY = 0;
                int totalZ = 0;
                int totalA = 0;
                int totalInCluster = 0;
                for(int j = 0; j < dataSet.size(); j++)
                {
                    if(dataSet.get(j).cluster() == i){
                        totalX += dataSet.get(j).X();
                        totalY += dataSet.get(j).Y();
                        totalZ += dataSet.get(j).Z();
                        totalA += dataSet.get(j).A();
                        totalInCluster++;
                    }
                }
                if(totalInCluster > 0){
                    centroids.get(i).X(totalX / totalInCluster);
                    centroids.get(i).Y(totalY / totalInCluster);
                    centroids.get(i).Z(totalZ / totalInCluster);
                    centroids.get(i).A(totalA / totalInCluster);
                }
            }
            
            // Assign all data to the new centroids
            isStillMoving = false;
            
            for(int i = 0; i < dataSet.size(); i++)
            {
                Data tempData = dataSet.get(i);
                minimum = bigNumber;
                for(int j = 0; j < NUM_CLUSTERS; j++)
                {
                    distance = dist(tempData, centroids.get(j));
                    if(distance < minimum){
                        minimum = distance;
                        cluster = j;
                    }
                }
                tempData.cluster(cluster);
                if(tempData.cluster() != cluster){
                    tempData.cluster(cluster);
                    isStillMoving = true;
                }
            }
        }
        return;
    }
     
    /**
     * // Calculate Euclidean distance.
     * @param d - Data object.
     * @param c - Centroid object.
     * @return - double value.
     */
    private static double dist(Data d, Centroid c)
    {
        return Math.sqrt(Math.pow((c.A() - d.A()), 2) + Math.pow((c.Z() - d.Z()), 2) + Math.pow((c.Y() - d.Y()), 2) + Math.pow((c.X() - d.X()), 2));
    }
    
    private static void getKorelasi() {
        double X = 0;
        double jumXi = 0;
        double jumYi = 0;
        double jumXiYi = 0;
        double jumXiXi = 0;
        double jumYiYi = 0;
        double nilaiR;
        
        for(int i=0; i< SAMPLES.length; i++) {
            for(int j=0; j< 1; j++) {
                X = SAMPLES[i][j];
            }
            //System.out.println(X);
            
            double XiYi = X * Y[i];
            double XiXi = Math.pow(X , 2);
            double YiYi = Math.pow(Y[i] , 2);
            
            jumXi = jumXi + X;
            jumYi = jumYi + Y[i];
            jumXiYi = jumXiYi + XiYi;
            jumXiXi = jumXiXi + XiXi;
            jumYiYi = jumYiYi + YiYi;
        }
        
        nilaiR = ((12 * jumXiYi) - (jumXi * jumYi)) / ( Math.pow(((12 * jumXiXi) - Math.pow( jumXi, 2)) * ((12 * jumYiYi) - Math.pow( jumYi, 2)), 0.5));
       
        System.out.println("Nilai R : " + df.format(nilaiR) + "   ");
    }   
    
    private static void getError() {
        double X = 0;
        double jumXiYi = 0;
        double MAPE;
        
        for(int i=0; i< SAMPLES.length; i++) {
            for(int j=0; j< 1; j++) {
                X = SAMPLES[i][j];
            }
            double XiYi =  Math.abs(X - Y[i]) / X;
            jumXiYi = jumXiYi + XiYi;
        }
        
        MAPE = jumXiYi / 9 * 100;
        
        System.out.println("MAPE : " + df.format(MAPE) + " %");
    }
    
    public static void main(String[] args)
    {   
        
        System.out.println("<== K - Means Clustering ==>\n");
        
        getSample();
        initialize();
        kMeanCluster();
        
        // Print out clustering results.
        for(int i = 0; i < NUM_CLUSTERS; i++)
        {   
            int no = i +1;
            System.out.println("Cluster " + no + " includes:");
            
            for(int j = 0; j < TOTAL_DATA; j++)
            {
                if(dataSet.get(j).cluster() == i){
                    System.out.println("     (" + dataSet.get(j).X() + ", " + dataSet.get(j).Y() + ", " + dataSet.get(j).Z() + ", " + dataSet.get(j).A() + ")");
                }
            } // j
            System.out.println();
        } // i
        
        // Print out centroid results.
        //System.out.println("Centroids finalized at:");
        for(int i = 0; i < NUM_CLUSTERS; i++)
        {
            System.out.println("     (" + centroids.get(i).X() + ", " + centroids.get(i).Y() + ", " + centroids.get(i).Z()+ ", " + centroids.get(i).A()+ ")");
        }
        System.out.print("\n");
        
        getKorelasi();
        getError();
        
        return;
    }
    
}