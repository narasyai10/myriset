/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.api;

import com.myriset.object.Feedback;
import com.myriset.repo.FeedbackRepository;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.jooby.Err;
import org.jooby.FlashScope;
import org.jooby.Jooby;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

/**
 *
 * @author Narasyai10
 */
public class FeedbackApi extends Jooby{
    { 
        /**
         *
         * Everything about your Pets.
         */
        path("/api", () -> {
            //Feedback
            post("/insertFeedback", req -> {
                FeedbackRepository dbFeedback = require(FeedbackRepository.class);
                Feedback feedback = req.body(Feedback.class);

                int id;
                boolean toReturn = false;

                id = dbFeedback.insert(feedback);

                if (id > 0) {
                    toReturn = true;
                }

                return (toReturn);
            });

            get("/getFeedbackAll", req -> {
                FeedbackRepository db = require(FeedbackRepository.class);

                int start = 0;
                int max = 50;
                
                return db.list(start, max);
            });

            get("/getFeedback", req -> {
                FeedbackRepository db = require(FeedbackRepository.class);

                int halaman = req.param("halaman").intValue(1);
                int start = (halaman-1)*8;
                int max = 8;
                
                return db.list(start, max);
            });


            /**
             *
             * Find Ikan by ID
             *
             * @param id Ikan ID.
             * @return Returns <code>200</code> with a single Ikan or <code>404</code>
             */
            get("/getFeedback/:id", req -> {
                FeedbackRepository dbFeedback = require(FeedbackRepository.class);

                int id = req.param("id").intValue();

                Feedback feedback = dbFeedback.findById(id);
                if (feedback == null) {
                    throw new Err(Status.NOT_FOUND);
                }
                return feedback;
            });

            /**
             * Delete Feedback berdasarkan ID.
             */
            delete("/deleteFeedback/:id", req -> {
                FeedbackRepository db = require(FeedbackRepository.class);
                int idFeedback = req.param("id").intValue();
                boolean status = false;

                if (!db.delete(idFeedback)) {
                    throw new Err(Status.NOT_FOUND);
                } else {
                    status = true;
                }

                return status;
            });
        });

        path("/api/max-hal/totalFeedback", () -> {
            /**
             * Mendapatkan maksimal halaman Features.
             */
            get(req -> {
                FeedbackRepository db = require(FeedbackRepository.class);
                double jumlahTotal;

                jumlahTotal = (double) db.totalFeedback();
                
                double halamanTotal = jumlahTotal/8;
            
                if((halamanTotal%1) > 0) {
                    halamanTotal = halamanTotal - (halamanTotal%1) + 1;
                }

                return (int) halamanTotal;
            });
        });
        
    }
}
