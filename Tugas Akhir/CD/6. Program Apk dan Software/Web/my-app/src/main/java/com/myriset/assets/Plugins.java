/*
 * Copyright 2018 Narasyai10.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myriset.assets;

import org.jooby.Jooby;

/**
 *
 * @author Narasyai10
 */
public class Plugins extends Jooby {
    {
        assets("/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js.map", "/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js.map");
        assets("/admin/plugins/bootstrap/css/bootstrap.css", "/admin/plugins/bootstrap/css/bootstrap.css");
        assets("/admin/plugins/node-waves/waves.css", "/admin/plugins/node-waves/waves.css");
        assets("/admin/plugins/animate-css/animate.css", "/admin/plugins/animate-css/animate.css");
        assets("/admin/plugins/morrisjs/morris.css", "/admin/plugins/morrisjs/morris.css");
        assets("/admin/plugins/jquery/jquery.min.js", "/admin/plugins/jquery/jquery.min.js");
        assets("/admin/plugins/bootstrap/js/bootstrap.js", "/admin/plugins/bootstrap/js/bootstrap.js");
        assets("/admin/plugins/bootstrap-select/js/bootstrap-select.js", "/admin/plugins/bootstrap-select/js/bootstrap-select.js");
        assets("/admin/plugins/jquery-slimscroll/jquery.slimscroll.js", "/admin/plugins/jquery-slimscroll/jquery.slimscroll.js");
        assets("/admin/plugins/node-waves/waves.js", "/admin/plugins/node-waves/waves.js");
        assets("/admin/plugins/jquery-countto/jquery.countTo.js", "/admin/plugins/jquery-countto/jquery.countTo.js");
        assets("/admin/plugins/raphael/raphael.min.js", "/admin/plugins/raphael/raphael.min.js");
        assets("/admin/plugins/morrisjs/morris.js", "/admin/plugins/morrisjs/morris.js");
        assets("/admin/plugins/chartjs/Chart.bundle.js", "/admin/plugins/chartjs/Chart.bundle.js");
        assets("/admin/plugins/flot-charts/jquery.flot.js", "/admin/plugins/flot-charts/jquery.flot.js");
        assets("/admin/plugins/flot-charts/jquery.flot.resize.js", "/admin/plugins/flot-charts/jquery.flot.resize.js");
        assets("/admin/plugins/flot-charts/jquery.flot.pie.js", "/admin/plugins/flot-charts/jquery.flot.pie.js");
        assets("/admin/plugins/flot-charts/jquery.flot.categories.js", "/admin/plugins/flot-charts/jquery.flot.categories.js");
        assets("/admin/plugins/flot-charts/jquery.flot.time.js", "/admin/plugins/flot-charts/jquery.flot.time.js");
        assets("/admin/plugins/jquery-sparkline/jquery.sparkline.js", "/admin/plugins/jquery-sparkline/jquery.sparkline.js");
        assets("/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js", "/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js");
        assets("/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js", "/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js");
        assets("/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js", "/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js");
        assets("/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js", "/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js");
        assets("/admin/plugins/jquery-datatable/jquery.dataTables.js", "/admin/plugins/jquery-datatable/jquery.dataTables.js");
        assets("/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css", "/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css");
        assets("/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js", "/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js");
        assets("/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js", "/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js");
        assets("/admin/plugins/jquery-datatable/extensions/export/jszip.min.js", "/admin/plugins/jquery-datatable/extensions/export/jszip.min.js");
        assets("/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js", "/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js");
        assets("/admin/plugins/bootstrap/fonts/glyphicons-halflings-regular.ttf", "/admin/plugins/bootstrap/fonts/glyphicons-halflings-regular.ttf");
        assets("/admin/plugins/bootstrap/fonts/glyphicons-halflings-regular.woff2", "/admin/plugins/bootstrap/fonts/glyphicons-halflings-regular.woff2");
        assets("/admin/plugins/bootstrap/fonts/glyphicons-halflings-regular.woff", "/admin/plugins/bootstrap/fonts/glyphicons-halflings-regular.woff");
        assets("/admin/plugins/jquery-validation/jquery.validate.js", "/admin/plugins/jquery-validation/jquery.validate.js");
        assets("/admin/plugins/bootstrap/css/bootstrap.css.map", "/admin/plugins/bootstrap/css/bootstrap.css.map");
        assets("/admin/plugins/sweetalert/sweetalert.css", "/admin/plugins/sweetalert/sweetalert.css");
        assets("/admin/plugins/sweetalert/sweetalert.min.js", "/admin/plugins/sweetalert/sweetalert.min.js");
        assets("/admin/plugins/chartjs/Chart.bundle.js", "/admin/plugins/chartjs/Chart.bundle.js");
        assets("/admin/plugins/morrisjs/morris.css", "/admin/plugins/morrisjs/morris.css");
        assets("/admin/plugins/jquery-countto/jquery.countTo.js", "/admin/plugins/jquery-countto/jquery.countTo.js");
        assets("/admin/plugins/raphael/raphael.min.js", "/admin/plugins/raphael/raphael.min.js");
        assets("/admin/plugins/morrisjs/morris.js", "/admin/plugins/morrisjs/morris.js");
        assets("/admin/plugins/bootstrap-notify/bootstrap-notify.js", "/admin/plugins/bootstrap-notify/bootstrap-notify.js");
        assets("/admin/plugins/bootstrap-select/js/bootstrap-select.js", "/admin/plugins/bootstrap-select/js/bootstrap-select.js");
    }
}
