package com.pens.narasyai10.fishery.api;

import com.pens.narasyai10.fishery.model.DaerahIkanBody;
import com.pens.narasyai10.fishery.model.EkonomiBody;
import com.pens.narasyai10.fishery.model.FeedbackBody;
import com.pens.narasyai10.fishery.model.IkanBody;
import com.pens.narasyai10.fishery.model.Response.ResponseEkonomi;
import com.pens.narasyai10.fishery.model.SaranaBody;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    // Fungsi ini untuk memanggil API http://192.168.43.230/api/insertFeedback
    @POST("insertFeedback")
    Call<ResponseBody> feedbackRequest(@Header("Content-Type") String header,
                                       @Body FeedbackBody body);

    // Fungsi ini untuk memanggil API http://192.168.43.230/api/getIkan
    @GET("getIkanAll")
    Call<List<IkanBody>> getIkan();

    // Fungsi ini untuk memanggil API http://192.168.43.230/api/getIkan
    @GET("getSaranaAll")
    Call<List<SaranaBody>> getSarana();

    // Fungsi ini untuk memanggil API http://192.168.43.230/api/getEkonomi
    @GET("getEkonomiAll")
    Call<List<ResponseEkonomi>> getEkonomi();

    // Fungsi ini untuk memanggil API http://192.168.43.230/api/getEkonomi
    @GET("getDaerahPerikananAll")
    Call<List<DaerahIkanBody>> getDaerah();

    // Fungsi ini untuk memanggil API http://192.168.43.230/api/getEkonomi
    @GET("filterDaerahPerikanan")
    Call<List<DaerahIkanBody>> getFilterDaerah(@Query("bulan") String bulan, @Query("tahun") String tahun, @Query("id_ling") String id_ling);

}