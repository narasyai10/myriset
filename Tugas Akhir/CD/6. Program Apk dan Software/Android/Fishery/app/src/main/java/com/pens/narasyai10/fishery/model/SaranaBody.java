package com.pens.narasyai10.fishery.model;

public class SaranaBody {
    private String namaSarana;
    private String jenisSarana;
    private String urlGambar;
    private String keterangan;

    public String getNamaSarana() {
        return namaSarana;
    }

    public void setNamaSarana(String namaSarana) {
        this.namaSarana = namaSarana;
    }

    public String getJenisSarana() {
        return jenisSarana;
    }

    public void setJenisSarana(String jenisSarana) {
        this.jenisSarana = jenisSarana;
    }

    public String getUrlGambar() {
        return urlGambar;
    }

    public void setUrlGambar(String urlGambar) {
        this.urlGambar = urlGambar;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
