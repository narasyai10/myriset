package com.pens.narasyai10.fishery.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pens.narasyai10.fishery.R;
import com.pens.narasyai10.fishery.model.IkanBody;

import java.util.List;

public class IkanAdapter extends RecyclerView.Adapter<IkanAdapter.ViewHolder> {
    private List<IkanBody> ikanList;
    private Context context;


    public IkanAdapter(Context context, List<IkanBody> ikanList) {
        this.ikanList = ikanList;
        this.context = context;
    }

    @Override
    public IkanAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_items_ikan, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final IkanAdapter.ViewHolder viewHolder, int i) {

        viewHolder.namaIkan.setText(ikanList.get(i).getNamaIkan());
        Glide.with(context).load(ikanList.get(i).getUrlGambar())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        viewHolder.progress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        viewHolder.progress.setVisibility(View.GONE);
                        return false;
                    }
                })
                .override(600,200)
                .thumbnail(0.5f)
                .crossFade()
                .into(viewHolder.imgUrl);
        viewHolder.musimIkan.setText(ikanList.get(i).getMusimIkan());
        viewHolder.perairan.setText(ikanList.get(i).getPerairan());
    }

    @Override
    public int getItemCount() {
        return ikanList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView namaIkan;
        private ImageView imgUrl;
        private TextView musimIkan;
        private TextView perairan;
        private ProgressBar progress;

        public ViewHolder(View view) {
            super(view);

            namaIkan = (TextView)view.findViewById(R.id.txtNama);
            imgUrl = (ImageView) view.findViewById(R.id.imgUrl);
            musimIkan = (TextView) view.findViewById(R.id.txtMusim);
            perairan = (TextView) view.findViewById(R.id.txtPerairan);
            progress = (ProgressBar) view.findViewById(R.id.progressBarIkan);
        }
    }
}