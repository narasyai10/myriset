package com.pens.narasyai10.fishery.model;

public class FeedbackBody {
    private String nama;
    private String email;
    private String subjek;
    private String feedback;

    public FeedbackBody(String nama, String email, String subjek, String feedback) {
        this.nama = nama;
        this.email = email;
        this.subjek = subjek;
        this.feedback = feedback;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubjek() {
        return subjek;
    }

    public void setSubjek(String subjek) {
        this.subjek = subjek;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
